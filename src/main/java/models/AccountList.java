package models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class AccountList {
    private ArrayList<AdminAccount> adminAccounts;
    private ArrayList<StaffAccount> staffAccounts;
    private ArrayList<UserAccount> userAccounts;
    private Account currentAccount;

    public AccountList(){
        adminAccounts = new ArrayList<>();
        staffAccounts = new ArrayList<>();
        userAccounts = new ArrayList<>();
    }

    public void addAdmin(AdminAccount adminAccount){ adminAccounts.add(adminAccount); }
    public void addStaff(StaffAccount staffAccount){ staffAccounts.add(staffAccount); }
    public void addUser(UserAccount userAccount){ userAccounts.add(userAccount); }


    public ArrayList<AdminAccount> getAdminAccounts() { return adminAccounts; }
    public ArrayList<StaffAccount> getStaffAccounts() {
        return staffAccounts;
    }
    public ArrayList<UserAccount> getUserAccounts() {
        return userAccounts;
    }
    public Account getCurrentAccount() { return currentAccount; }

    public void setAdminAccounts(ArrayList<AdminAccount> adminAccounts) { this.adminAccounts = adminAccounts; }
    public void setStaffAccounts(ArrayList<StaffAccount> staffAccounts) { this.staffAccounts = staffAccounts; }
    public void setUserAccounts(ArrayList<UserAccount> userAccounts) { this.userAccounts = userAccounts; }
    public void setCurrentAccount(Account currentAccount) { this.currentAccount = currentAccount; }

    public boolean checkAccountAlreadyUse(String username){
        for(AdminAccount acc: adminAccounts){
            if(acc.getUsername().equals(username)){
                return true;
            }
        }
        for(StaffAccount acc: staffAccounts){
            if(acc.getUsername().equals(username)){
                return true;
            }
        }
        for(UserAccount acc: userAccounts){
            if(acc.getUsername().equals(username)){
                return true;
            }
        }
        return false;
    }

}
