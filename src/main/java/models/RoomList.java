package models;

import java.util.ArrayList;

public class RoomList {
    private ArrayList<Room> rooms;

    public RoomList() {
        rooms = new ArrayList<>();
    }
    public boolean checkAlreadyHaveRoom(String room){
        for(Room r :rooms){
            if(r.getRoomNumber().equals(room)){
                return true;
            }
        }
        return false;
    }
    public boolean checkResidentRoom(String room,String name,String lastName){
        for(Room r : rooms){
            String resident = name + " " + lastName;
            if (r.getRoomNumber().equals(room) && r.checkResident(resident)){
                return true;
            }
        }
        return false;
    }
    public boolean checkRoom(String roomNumber){
        for(Room r : rooms){
            if (r.getRoomNumber().equals(roomNumber)){
                return true;
            }
        }
        return false;
    }
    public boolean checkResidentInRoom(String roomNumber){
        for(Room r : rooms){
            if (r.getRoomNumber().equals(roomNumber)){
                if (r.getRoomType().equals("One bedroom") && r.getResidents().size() < 2){
                    return true;
                }else if (r.getRoomType().equals("Two bedroom") && r.getResidents().size() < 4) {
                    return true;
                }
            }
        }
        return false;
    }

    public void addRoom(Room room){
        rooms.add(room);
    }

    public ArrayList<Room> getRooms() {
        return rooms;
    }
}
