package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import models.Room;
import services.AccountFileDataSource;
import services.MailFileDataSource;
import services.RoomFileDataSource;

import java.io.IOException;

public class RoomInfo {
    @FXML ComboBox<String> room,roomType,building,floor;
    @FXML Button enter, back;
    @FXML ImageView background;
    private String roomNumber;
    private AccountFileDataSource accountFileDataSource;
    private RoomFileDataSource roomFileDataSource;
    private MailFileDataSource mailFileDataSource;

    public void setAccountFileDataSource(AccountFileDataSource accountFileDataSource) {
        this.accountFileDataSource = accountFileDataSource;
    }
    public void setRoomFileDataSource(RoomFileDataSource roomFileDataSource) {
        this.roomFileDataSource = roomFileDataSource;
    }
    public void setMailFileDataSource(MailFileDataSource mailFileDataSource) {
        this.mailFileDataSource = mailFileDataSource;
    }

    @FXML public void initialize(){
        background.setImage(new Image("/images/juli-moreira.jpg"));
        roomType.getItems().addAll("One bedroom","Two bedroom");
        building.getItems().addAll("A","B","C");
        floor.getItems().addAll("1","2","3","4","5","6","7","8","9");
        room.getItems().addAll("1","2","3","4","5","6","7","8","9");
    }
    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manage_resident.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        ManageResident controller = loader.getController();
        controller.setAccountFileDataSource(accountFileDataSource);
        controller.setRoomFileDataSource(roomFileDataSource);
        controller.setMailFileDataSource(mailFileDataSource);

        stage.show();
    }
    @FXML public void handleEnterOnAction(ActionEvent event) throws IOException {
        roomNumber = building.getSelectionModel().getSelectedItem() + floor.getSelectionModel().getSelectedItem() + room.getSelectionModel().getSelectedItem();
        if (roomType.getSelectionModel().isEmpty() || room.getSelectionModel().isEmpty() || floor.getSelectionModel().isEmpty() || building.getSelectionModel().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error!!");
            alert.setHeaderText(null);
            alert.setContentText("Please check your information and try again.");

            alert.showAndWait();
        }

        else if (roomFileDataSource.getRoomList().checkRoom(roomNumber)){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Room Error!!");
            alert.setHeaderText(null);
            alert.setContentText("This room was already added.");

            alert.showAndWait();
        } else if (!roomFileDataSource.getRoomList().checkRoom(roomNumber)){
            roomFileDataSource.newRoom(new Room(roomNumber, roomType.getSelectionModel().getSelectedItem()));

            Button e = (Button) event.getSource();
            Stage stage = (Stage) e.getScene().getWindow();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/room_info.fxml"));
            stage.setScene(new Scene(loader.load(),1280,800));
            RoomInfo controller = loader.getController();
            controller.setAccountFileDataSource(accountFileDataSource);
            controller.setRoomFileDataSource(roomFileDataSource);
            controller.setMailFileDataSource(mailFileDataSource);

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Success!!");
            alert.setHeaderText(null);
            alert.setContentText("Add room complete.");

            alert.showAndWait();

            stage.show();

        }
    }
}
