package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import services.AccountFileDataSource;
import services.MailFileDataSource;
import services.RoomFileDataSource;

import java.io.IOException;


public class ManageMail {
    @FXML Button listOfMails, mailForm, back;
    @FXML ImageView background;
    private AccountFileDataSource accountFileDataSource;
    private RoomFileDataSource roomFileDataSource;
    private MailFileDataSource mailFileDataSource;

    public void setAccountFileDataSource(AccountFileDataSource accountFileDataSource) {
        this.accountFileDataSource = accountFileDataSource;
    }
    public void setRoomFileDataSource(RoomFileDataSource roomFileDataSource) {
        this.roomFileDataSource = roomFileDataSource;
    }
    public void setMailFileDataSource(MailFileDataSource mailFileDataSource) {
        this.mailFileDataSource = mailFileDataSource;
    }

    @FXML public void initialize() throws IOException {
        background.setImage(new Image("/images/halacious.jpg"));
    }
    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/system_for_staff.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        SystemForStaff controller = loader.getController();
        controller.setAccountFileDataSource(accountFileDataSource);
        controller.setRoomFileDataSource(roomFileDataSource);
        controller.setMailFileDataSource(mailFileDataSource);

        stage.show();
    }
    @FXML public void handleListOfMailsOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/list_of_mails.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        ListOfMails controller = loader.getController();
        controller.setAccountFileDataSource(accountFileDataSource);
        controller.setRoomFileDataSource(roomFileDataSource);
        controller.setMailFileDataSource(mailFileDataSource);
        controller.setTable(null);

        stage.show();
    }
    @FXML public void handleMailFormOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/mail_form.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        MailForm controller = loader.getController();
        controller.setAccountFileDataSource(accountFileDataSource);
        controller.setRoomFileDataSource(roomFileDataSource);
        controller.setMailFileDataSource(mailFileDataSource);

        stage.show();
    }
}
