package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;

public class Menu {
    @FXML Button myProfile, recommend, login;
    @FXML ImageView background;

    @FXML public void initialize() {
            background.setImage(new Image("/images/pink sky.jpg"));
    }
    @FXML public void handleRecommendOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/recommend.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        stage.show();
    }
    @FXML public void handleMyProfileOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/my_profile.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        stage.show();
    }
    @FXML public void handleLoginOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        stage.show();
    }
}
