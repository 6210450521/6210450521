package models;

import java.util.ArrayList;

public class MailList {
    private ArrayList<Mail> mails;

    public MailList(){
        mails = new ArrayList<>();
    }
    public void addMail(Mail mail){
        mails.add(mail);
    }

    public ArrayList<Mail> getMails() {
        return mails;
    }

    public void setMails(ArrayList<Mail> mails) {
        this.mails = mails;
    }

}
