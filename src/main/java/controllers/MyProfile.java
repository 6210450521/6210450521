package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;

public class MyProfile {
    @FXML Button back;
    @FXML ImageView background;
    @FXML ImageView profileImage;

    @FXML public void initialize() {
        background.setImage(new Image("/images/matt-le.jpg"));
        profileImage.setImage(new Image("/images/profileImage.JPG"));

    }
    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/menu.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        stage.show();
    }
}
