package services;

import models.*;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class AccountFileDataSource {
    private AccountList accountList;

    public AccountFileDataSource(){
        accountList = new AccountList();
    }

    public void checkFile(){
        accountList = new AccountList();
        File file = new File("Data");
        if (!file.exists()) {
            file.mkdirs();
        }

        checkAndReadAdminAccountFile();
        checkAndReadStaffAccountFile();
        checkAndReadUserAccountFile();
    }
    public void checkAndReadAdminAccountFile(){
        String filePath = "Data"+ File.separator + "Admin.csv";
        File file = new File(filePath);
        if (!file.exists()) {
            try{
                file.createNewFile();
            }catch(IOException e){
                System.out.println(e.getMessage());
            }
        }
        try{
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);
            String text = "";
            while((text = reader.readLine()) != null){
                String[] array = text.split(",");
                String username = array[0];
                String password = array[1];
                AdminAccount adminAccount = new AdminAccount(username,password);
                accountList.addAdmin(adminAccount);
            }
            reader.close();
            fileReader.close();
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }
    public void checkAndReadStaffAccountFile(){
        String filePath = "Data"+ File.separator + "Staff.csv";
        File file = new File(filePath);
        if (!file.exists()) {
            try{
                file.createNewFile();
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        try{
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);
            String text = "";
            while((text = reader.readLine()) != null){
                String[] array = text.split(",");
                String username = array[0];
                String password = array[1];
                String name = array[2];
                String lastName = array[3];
                String lastLogin = array[4];
                String status = array[5];
                String bannedLogin = array[6];
                String staffImage = array[7];
                StaffAccount staffAccount = new StaffAccount(username,password,name,lastName);
                if (!lastLogin.equals("-")){
                    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
                    LocalDateTime dateTime = LocalDateTime.parse(lastLogin,dateTimeFormatter);
                    staffAccount.setLastLogin(dateTime);
                }
                    staffAccount.setImage(staffImage);
                staffAccount.setStatus(status);
                staffAccount.setBannedLogin(Integer.valueOf(bannedLogin));
                accountList.addStaff(staffAccount);
            }
            reader.close();
            fileReader.close();
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }
    public void checkAndReadUserAccountFile(){
        String filePath = "Data"+ File.separator + "User.csv";
        File file = new File(filePath);
        if (!file.exists()) {
            try{
                file.createNewFile();
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        try{
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);
            String text = "";
            while((text = reader.readLine()) != null){
                String[] array = text.split(",");
                String room = array[0];
                String name = array[1];
                String lastName = array[2];
                String username = array[3];
                String password = array[4];
                UserAccount userAccount = new UserAccount(new Resident(name,lastName,room),username,password);
                accountList.addUser(userAccount);
            }
            reader.close();
            fileReader.close();
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    public void newStaffAccount(StaffAccount staffAccount){
        staffAccount.setStatus("Not banned");
        accountList.addStaff(staffAccount);
        try{
            FileWriter fileWriter = new FileWriter("Data"+ File.separator + "Staff.csv", true);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            String username = staffAccount.getUsername();
            String password = staffAccount.getPassword();
            String name = staffAccount.getName();
            String lastName = staffAccount.getLastName();
            String lastLogin = "-";
            String status = "Not banned";
            String bannedLogin = "0";
            String staffImage;
            if (staffAccount.getImage() == null){
                staffImage = "-";
            }else{
                staffImage = staffAccount.getImage();
            }

            writer.append(username + "," + password +","+ name + "," + lastName +
                    "," + lastLogin + "," + status+","+bannedLogin + ","+staffImage);
            writer.newLine();
            writer.close();
            fileWriter.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    public void newUserAccount(UserAccount userAccount){

        try{
            FileWriter fileWriter = new FileWriter("Data"+ File.separator + "User.csv", true);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            String room = userAccount.getResident().getRoomNumber();
            String name = userAccount.getResident().getName();
            String lastName = userAccount.getResident().getLastName();
            String username = userAccount.getUsername();
            String password = userAccount.getPassword();
            writer.append(room+","+ name +","+ lastName + ","+ username+ "," + password);
            writer.newLine();
            writer.close();
            fileWriter.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public void changeAdminPassword(AdminAccount adminAccount){
        for(AdminAccount acc: accountList.getAdminAccounts()){
            if (acc.getUsername().equals(accountList.getCurrentAccount().getUsername())){
                acc.setPassword(adminAccount.getPassword());
                accountList.getCurrentAccount().setPassword(adminAccount.getPassword());
            }
        }
        try{
            FileWriter fileWriter = new FileWriter("Data"+ File.separator + "Admin.csv");
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for(int i=0; i < accountList.getAdminAccounts().size();i++){
                String username = accountList.getAdminAccounts().get(i).getUsername();
                String password = accountList.getAdminAccounts().get(i).getPassword();
                writer.write(username + "," + password);
                writer.newLine();
            }
            writer.close();
            fileWriter.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    public void changeStaffPassword(StaffAccount staffAccount) {
        for (StaffAccount acc : accountList.getStaffAccounts()) {
            if (acc.getUsername().equals(accountList.getCurrentAccount().getUsername())) {
                acc.setPassword(staffAccount.getPassword());
                accountList.getCurrentAccount().setPassword(staffAccount.getPassword());
            }
        }
        updateStaff();
    }
    public void changeUserPassword(UserAccount userAccount){
        for(UserAccount acc: accountList.getUserAccounts()){
            if (acc.getUsername().equals(accountList.getCurrentAccount().getUsername())){
                acc.setPassword(userAccount.getPassword());
                accountList.getCurrentAccount().setPassword(userAccount.getPassword());
            }
        }
        updateUser();
    }

    public AccountList getAccountList(){
        return accountList;
    }

    public void setTimeToStaff(StaffAccount staffAccount){
        staffAccount.setLastLogin(LocalDateTime.now());
        updateStaff();
    }
    public void updateStaff(){
        try{
            FileWriter fileWriter = new FileWriter("Data"+ File.separator + "Staff.csv");
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for(int i=0; i < accountList.getStaffAccounts().size();i++){
                String username = accountList.getStaffAccounts().get(i).getUsername();
                String password = accountList.getStaffAccounts().get(i).getPassword();
                String name = accountList.getStaffAccounts().get(i).getName();
                String lastName = accountList.getStaffAccounts().get(i).getLastName();
                String lastLogin = accountList.getStaffAccounts().get(i).getLastLogin();
                String status = accountList.getStaffAccounts().get(i).getStatus();
                int bannedLogin = accountList.getStaffAccounts().get(i).getBannedLogin();
                String b = String.valueOf(bannedLogin);
                String staffImage = accountList.getStaffAccounts().get(i).getImage();
                writer.write(username + ","+ password + "," + name + "," + lastName +","+ lastLogin + "," + status + "," + b + "," + staffImage);
                writer.newLine();
            }
            writer.close();
            fileWriter.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    public void updateUser(){
        try{
            FileWriter fileWriter = new FileWriter("Data"+ File.separator + "User.csv");
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for(int i=0; i < accountList.getUserAccounts().size();i++){
                String roomNumber = accountList.getUserAccounts().get(i).getResident().getRoomNumber();
                String name = accountList.getUserAccounts().get(i).getResident().getName();
                String lastName = accountList.getUserAccounts().get(i).getResident().getLastName();
                String username = accountList.getUserAccounts().get(i).getUsername() ;
                String password = accountList.getUserAccounts().get(i).getPassword();
                writer.write(roomNumber + "," + name+ "," + lastName + "," + username + "," + password);
                writer.newLine();
            }
            writer.close();
            fileWriter.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
