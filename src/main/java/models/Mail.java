package models;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Mail {
    private Resident resident;
    private String postman;
    private String width;
    private String height;
    private String image;
    private LocalDateTime sendDateTime;
    private String status;
    private String staffReceiver;
    private String residentReceiver;
    private LocalDateTime receiveDateTime;
    private String receiveFromStaff;

    public Mail(Resident resident, String postman, String width, String height, String image, String status){
        this.resident = resident;
        this.postman = postman;
        this.width = width;
        this.height = height;
        this.image = image;
        this.status = status;
    }

    public Resident getResident() { return resident; }
    public String getName(){ return resident.getName(); }
    public String getLastName(){ return resident.getLastName(); }
    public String getRoomNumber(){ return resident.getRoomNumber(); }
    public String getHeight() { return height; }
    public String getWidth() { return width; }
    public String getPostman() { return postman; }
    public String getImage() { return image; }
    public String getStatus() { return status; }
    public String getType(){ return "Mail"; }
    public String getPriority(){ return ""; }
    public String getCompany(){ return ""; }
    public String getTrackingNumber(){ return "";}
    public String getSendDateTime() {
        if (sendDateTime == null){
            return "-";
        }
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
        String formattedDateTime = sendDateTime.format(dateTimeFormatter);
        return formattedDateTime;
    }

    public String getReceiveDateTime() {
        if (receiveDateTime == null){
            return "-";
        }
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
        String formattedDateTime = receiveDateTime.format(dateTimeFormatter);
        return formattedDateTime;
    }

    public String getResidentReceiver() {
        return residentReceiver;
    }
    public String getStaffReceiver() {
        return staffReceiver;
    }
    public String getReceiveFromStaff() { return receiveFromStaff; }

    public void setSendDateTime(LocalDateTime sendDateTime) { this.sendDateTime = sendDateTime; }
    public void setStatus(String status) { this.status = status; }
    public void setReceiveDateTime(LocalDateTime receiveDateTime) {
        this.receiveDateTime = receiveDateTime;
    }
    public void setResidentReceiver(String residentReceiver) {
        this.residentReceiver = residentReceiver;
    }
    public void setStaffReceiver(String staffReceiver) {
        this.staffReceiver = staffReceiver;
    }
    public void setReceiveFromStaff(String receiveFromStaff) { this.receiveFromStaff = receiveFromStaff; }
}
