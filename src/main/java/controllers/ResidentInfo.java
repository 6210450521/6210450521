package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import models.Room;
import services.AccountFileDataSource;
import services.MailFileDataSource;
import services.RoomFileDataSource;

import java.io.IOException;

public class ResidentInfo {
    @FXML TextField name,lastName, roomNumber;
    @FXML Button back,enter;
    @FXML ImageView background;
    private AccountFileDataSource accountFileDataSource;
    private RoomFileDataSource roomFileDataSource;
    private MailFileDataSource mailFileDataSource;

    public void setAccountFileDataSource(AccountFileDataSource accountFileDataSource) {
        this.accountFileDataSource = accountFileDataSource;
    }
    public void setRoomFileDataSource(RoomFileDataSource room) {
        this.roomFileDataSource = room;
    }
    public void setMailFileDataSource(MailFileDataSource mailFileDataSource) {
        this.mailFileDataSource = mailFileDataSource;
    }

    @FXML public void initialize(){
        background.setImage(new Image("/images/lamp.jpg"));
    }
    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manage_resident.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        ManageResident controller = loader.getController();
        controller.setAccountFileDataSource(accountFileDataSource);
        controller.setRoomFileDataSource(roomFileDataSource);
        controller.setMailFileDataSource(mailFileDataSource);

        stage.show();
    }
    @FXML public void handleEnterOnAction(ActionEvent event) throws IOException {


        if (name.getText().equals("") || lastName.getText().equals("") || roomNumber.getText().equals("")){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error!!");
            alert.setHeaderText(null);
            alert.setContentText("Please check your information and try again.");

            alert.showAndWait();
        }else {
            if (roomFileDataSource.getRoomList().checkRoom(roomNumber.getText()) && roomFileDataSource.getRoomList().checkResidentInRoom(roomNumber.getText())){
                Button e = (Button) event.getSource();
                Stage stage = (Stage) e.getScene().getWindow();

                FXMLLoader loader = new FXMLLoader(getClass().getResource("/resident_info.fxml"));
                stage.setScene(new Scene(loader.load(),1280,800));

                ResidentInfo controller = loader.getController();

                roomFileDataSource.addResident(roomNumber.getText(), name.getText() + " " + lastName.getText());
                controller.setAccountFileDataSource(accountFileDataSource);
                controller.setRoomFileDataSource(roomFileDataSource);
                controller.setMailFileDataSource(mailFileDataSource);

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Success!!");
                alert.setHeaderText(null);
                alert.setContentText("Add resident complete.");

                alert.showAndWait();

                stage.show();
            }else if(!roomFileDataSource.getRoomList().checkRoom(roomNumber.getText())){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Room Error!!");
                alert.setHeaderText(null);
                alert.setContentText("Please check room number.");

                alert.showAndWait();
            }else if(!roomFileDataSource.getRoomList().checkResidentInRoom(roomNumber.getText())){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Room Error!!");
                alert.setHeaderText(null);
                alert.setContentText("This room is full.");

                alert.showAndWait();
            }
        }


    }
}
