package models;

public class Parcel extends Mail{
    private String company;
    private String trackingNumber;

    public Parcel(Resident resident, String postman, String width, String height, String image, String status , String company, String trackingNumber) {
        super(resident, postman, width, height, image, status);
        this.company = company;
        this.trackingNumber = trackingNumber;
    }

    public String getCompany() { return company; }
    public String getTrackingNumber() { return trackingNumber; }
    public String getType(){
        return "Parcel";
    }

}
