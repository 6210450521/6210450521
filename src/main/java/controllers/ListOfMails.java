package controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import models.Mail;
import models.Room;
import services.AccountFileDataSource;
import services.MailFileDataSource;
import services.RoomFileDataSource;
import services.StringConfiguration;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class ListOfMails {
    @FXML TableView<Mail> table;
    @FXML ComboBox<String> sortBy,tableType;
    @FXML TextField search,receiverName,receiverLastName;
    @FXML ImageView background, mailImage;
    @FXML Button receive,back,selectTableType;
    private AccountFileDataSource accountFileDataSource;
    private RoomFileDataSource roomFileDataSource;
    private MailFileDataSource mailFileDataSource;
    private Mail selectedMail;
    private ObservableList<Mail> mailObservableList;
    private ArrayList<Mail> notReceivedMail;
    private TableColumn dateTimeColumn, roomNumberColumn;

    public void setAccountFileDataSource(AccountFileDataSource accountFileDataSource) {
        this.accountFileDataSource = accountFileDataSource;
    }
    public void setRoomFileDataSource(RoomFileDataSource roomFileDataSource) {
        this.roomFileDataSource = roomFileDataSource;
    }
    public void setMailFileDataSource(MailFileDataSource mailFileDataSource) {
        this.mailFileDataSource = mailFileDataSource;
    }

    @FXML public void initialize(){
        background.setImage(new Image("/images/Joanna-Kosinska.jpg"));
        notReceivedMail = new ArrayList<>();
        receive.setDisable(true);
        receiverName.setDisable(true);
        receiverLastName.setDisable(true);
        sortBy.getItems().setAll("Latest mail","Ascending room number","Descending room number");
        tableType.getItems().setAll("Mail","Document","Parcel");
    }
    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manage_mail.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        ManageMail controller = loader.getController();
        controller.setAccountFileDataSource(accountFileDataSource);
        controller.setRoomFileDataSource(roomFileDataSource);
        controller.setMailFileDataSource(mailFileDataSource);

        stage.show();
    }
    @FXML public void handleReceiveOnAction(ActionEvent event) throws IOException{
        for(Room r: roomFileDataSource.getRoomList().getRooms()){
            if (r.getRoomNumber().equals(selectedMail.getRoomNumber()) && r.getResidents().contains(receiverName.getText() + " " + receiverLastName.getText())){
                selectedMail.setReceiveFromStaff(accountFileDataSource.getAccountList().getCurrentAccount().getName());
                selectedMail.setReceiveDateTime(LocalDateTime.now());
                mailFileDataSource.receiveMail(selectedMail,receiverName.getText());
            }
        }

        if (receiverName.getText() == null || receiverLastName == null){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error!!");
            alert.setHeaderText(null);
            alert.setContentText("Please check your information and try again.");
            alert.showAndWait();
        }else if (selectedMail.getResidentReceiver().equals("Empty")){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error!!");
            alert.setHeaderText(null);
            alert.setContentText("Not found your name in this room.");
            alert.showAndWait();
        }else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Success!!");
            alert.setHeaderText(null);
            alert.setContentText("Receive mail complete.");
            alert.showAndWait();

            Button e = (Button) event.getSource();
            Stage stage = (Stage) e.getScene().getWindow();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/list_of_mails.fxml"));
            stage.setScene(new Scene(loader.load(),1280,800));

            ListOfMails controller = loader.getController();
            controller.setAccountFileDataSource(accountFileDataSource);
            controller.setRoomFileDataSource(roomFileDataSource);
            controller.setMailFileDataSource(mailFileDataSource);
            controller.setTable(null);

            stage.show();
        }
    }
    public void showMailData(String mailType){
        if(mailType == null || mailType.equals("Mail")){
            for(Mail m : mailFileDataSource.getMailList().getMails()){
                if(m.getStatus().equals("Not received") && m.getType().equals("Mail")){
                    notReceivedMail.add(m);
                }
            }
            mailObservableList = FXCollections.observableArrayList(notReceivedMail);
            table.setItems(mailObservableList);

            ArrayList<StringConfiguration> configs = new ArrayList<>();
            configs.add(new StringConfiguration("title:Room number", "field:roomNumber"));
            configs.add(new StringConfiguration("title:Receiver name", "field:name"));
            configs.add(new StringConfiguration("title:Sender name", "field:postman"));
            configs.add(new StringConfiguration("title:Width", "field:width"));
            configs.add(new StringConfiguration("title:Height", "field:height"));
            configs.add(new StringConfiguration("title:Send date time", "field:sendDateTime"));

            TableColumn roomNumberCol = new TableColumn(configs.get(0).get("title"));
            TableColumn receiverNameCol = new TableColumn(configs.get(1).get("title"));
            TableColumn senderNameCol = new TableColumn(configs.get(2).get("title"));
            TableColumn widthCol = new TableColumn(configs.get(3).get("title"));
            TableColumn heightCol = new TableColumn(configs.get(4).get("title"));
            TableColumn dateTimeCol = new TableColumn(configs.get(5).get("title"));

            roomNumberCol.setPrefWidth(150);
            receiverNameCol.setPrefWidth(150);
            senderNameCol.setPrefWidth(150);
            widthCol.setPrefWidth(150);
            heightCol.setPrefWidth(150);
            heightCol.setPrefWidth(150);
            dateTimeCol.setPrefWidth(150);

            roomNumberCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(0).get("field")));
            receiverNameCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(1).get("field")));
            senderNameCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(2).get("field")));
            widthCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(3).get("field")));
            heightCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(4).get("field")));
            dateTimeCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(5).get("field")));

            dateTimeColumn = dateTimeCol;
            roomNumberColumn = roomNumberCol;

            table.getColumns().clear();
            table.getColumns().addAll(roomNumberCol,receiverNameCol,senderNameCol,widthCol,heightCol,dateTimeCol);

            dateTimeCol.setSortType(TableColumn.SortType.DESCENDING);
            table.getSortOrder().setAll(dateTimeCol);
        }else if (mailType.equals("Document")){
            for(Mail m : mailFileDataSource.getMailList().getMails()){
                if(m.getStatus().equals("Not received") && m.getType().equals("Document")){
                    notReceivedMail.add(m);
                }
            }
            mailObservableList = FXCollections.observableArrayList(notReceivedMail);
            table.setItems(mailObservableList);
            table.getColumns().clear();

            ArrayList<StringConfiguration> configs = new ArrayList<>();
            configs.add(new StringConfiguration("title:Room number", "field:roomNumber"));
            configs.add(new StringConfiguration("title:Receiver name", "field:name"));
            configs.add(new StringConfiguration("title:Sender name", "field:postman"));
            configs.add(new StringConfiguration("title:Width", "field:width"));
            configs.add(new StringConfiguration("title:Height", "field:height"));
            configs.add(new StringConfiguration("title:Priority", "field:priority"));
            configs.add(new StringConfiguration("title:Send date time", "field:sendDateTime"));

            TableColumn roomNumberCol = new TableColumn(configs.get(0).get("title"));
            TableColumn receiveNameCol = new TableColumn(configs.get(1).get("title"));
            TableColumn senderNameCol = new TableColumn(configs.get(2).get("title"));
            TableColumn widthCol = new TableColumn(configs.get(3).get("title"));
            TableColumn heightCol = new TableColumn(configs.get(4).get("title"));
            TableColumn priorityCol = new TableColumn(configs.get(5).get("title"));
            TableColumn dateTimeCol = new TableColumn(configs.get(6).get("title"));

            roomNumberCol.setPrefWidth(150);
            receiveNameCol.setPrefWidth(150);
            senderNameCol.setPrefWidth(150);
            widthCol.setPrefWidth(150);
            heightCol.setPrefWidth(150);
            priorityCol.setPrefWidth(150);
            dateTimeCol.setPrefWidth(150);

            roomNumberCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(0).get("field")));
            receiveNameCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(1).get("field")));
            senderNameCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(2).get("field")));
            widthCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(3).get("field")));
            heightCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(4).get("field")));
            priorityCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(5).get("field")));
            dateTimeCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(6).get("field")));

            dateTimeColumn = dateTimeCol;
            roomNumberColumn = roomNumberCol;

            table.getColumns().addAll(roomNumberCol,receiveNameCol,senderNameCol,widthCol,heightCol,priorityCol,dateTimeCol);

            dateTimeCol.setSortType(TableColumn.SortType.DESCENDING);
            table.getSortOrder().setAll(dateTimeCol);
        }else if (mailType.equals("Parcel")){
            for(Mail m : mailFileDataSource.getMailList().getMails()){
                if(m.getStatus().equals("Not received") && m.getType().equals("Parcel")){
                    notReceivedMail.add(m);
                }
            }
            mailObservableList = FXCollections.observableArrayList(notReceivedMail);
            table.setItems(mailObservableList);
            table.getColumns().clear();

            ArrayList<StringConfiguration> configs = new ArrayList<>();
            configs.add(new StringConfiguration("title:Room number", "field:roomNumber"));
            configs.add(new StringConfiguration("title:Receiver name", "field:name"));
            configs.add(new StringConfiguration("title:Sender name", "field:postman"));
            configs.add(new StringConfiguration("title:Width", "field:width"));
            configs.add(new StringConfiguration("title:Height", "field:height"));
            configs.add(new StringConfiguration("title:Company", "field:company"));
            configs.add(new StringConfiguration("title:Tracking number", "field:trackingNumber"));
            configs.add(new StringConfiguration("title:Send date time", "field:sendDateTime"));

            TableColumn roomNumberCol = new TableColumn(configs.get(0).get("title"));
            TableColumn receiverNameCol = new TableColumn(configs.get(1).get("title"));
            TableColumn senderNameCol = new TableColumn(configs.get(2).get("title"));
            TableColumn widthCol = new TableColumn(configs.get(3).get("title"));
            TableColumn heightCol = new TableColumn(configs.get(4).get("title"));
            TableColumn companyCol = new TableColumn(configs.get(5).get("title"));
            TableColumn trackingNumberCol = new TableColumn(configs.get(6).get("title"));
            TableColumn dateTimeCol = new TableColumn(configs.get(7).get("title"));


            roomNumberCol.setPrefWidth(150);
            receiverNameCol.setPrefWidth(150);
            senderNameCol.setPrefWidth(150);
            widthCol.setPrefWidth(150);
            heightCol.setPrefWidth(150);
            companyCol.setPrefWidth(150);
            trackingNumberCol.setPrefWidth(150);
            dateTimeCol.setPrefWidth(150);

            roomNumberCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(0).get("field")));
            receiverNameCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(1).get("field")));
            senderNameCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(2).get("field")));
            widthCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(3).get("field")));
            heightCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(4).get("field")));
            companyCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(5).get("field")));
            trackingNumberCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(6).get("field")));
            dateTimeCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(7).get("field")));

            dateTimeColumn = dateTimeCol;
            roomNumberColumn = roomNumberCol;

            table.getColumns().addAll(roomNumberCol,receiverNameCol,senderNameCol,widthCol,heightCol,companyCol,trackingNumberCol,dateTimeCol);

            dateTimeCol.setSortType(TableColumn.SortType.DESCENDING);
            table.getSortOrder().setAll(dateTimeCol);
        }
    }
    public void setTable(String sortType){
        showMailData(sortType);
        table.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                showSelectedMail(newValue);
                if (selectedMail.getStatus().equals("Not received")){
                    receive.setDisable(false);
                    receiverName.setDisable(false);
                    receiverLastName.setDisable(false);
                }
            }
        });
        searchMailByRoom();
    }

    private void showSelectedMail(Mail mail) {
        selectedMail = mail;
        mailImage.setImage(null);
        if (!selectedMail.getImage().equals("-")){
            try {
                mailImage.setImage(new Image("/mailsImage/" + selectedMail.getImage()));
            }catch (IllegalArgumentException e){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Load image Error!!");
                alert.setHeaderText(null);
                alert.setContentText("Can't load this image, Please close program and try again.");

                alert.showAndWait();
            }
        }
    }

    public void searchMailByRoom(){
        FilteredList<Mail> filteredData = new FilteredList<>(mailObservableList, p -> true);

        search.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(mail -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String filter = newValue;

                if (mail.getResident().getRoomNumber().contains(filter)) {
                    return true;
                }
                return false;
            });
        });

        SortedList<Mail> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(table.comparatorProperty());

        table.setItems(sortedData);
    }
    @FXML public void handleSelectTableTypeOnAction(ActionEvent event) throws IOException{
        table.getColumns().clear();
        notReceivedMail.clear();
        setTable(tableType.getSelectionModel().getSelectedItem());
    }

    @FXML public void handleSelectSortTypeOnAction(ActionEvent event) throws IOException{
        if (sortBy.getSelectionModel().getSelectedItem().equals("Latest mail")){
            dateTimeColumn.setSortType(TableColumn.SortType.DESCENDING);
            table.getSortOrder().setAll(dateTimeColumn);
        }else if (sortBy.getSelectionModel().getSelectedItem().equals("Ascending room number")){
            roomNumberColumn.setSortType(TableColumn.SortType.ASCENDING);
            table.getSortOrder().setAll(roomNumberColumn);
        }else if (sortBy.getSelectionModel().getSelectedItem().equals("Descending room number")){
            roomNumberColumn.setSortType(TableColumn.SortType.DESCENDING);
            table.getSortOrder().setAll(roomNumberColumn);
        }
    }
}
