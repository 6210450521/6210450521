package models;

public class UserAccount extends Account{
    private Resident resident;

    public UserAccount(Resident resident, String username, String password) {
        super(username, password);
        this.resident = resident;
    }

    public Resident getResident() { return resident; }

    public void setResident(Resident resident) { this.resident = resident; }
}
