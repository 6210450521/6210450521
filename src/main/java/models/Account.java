package models;

public class Account {
    private String username;
    private String password;

    public Account(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() { return username; }
    public String getPassword() { return password; }
    public String getName() {
        return null;
    }
    public String getLastName() {
        return null;
    }
    public Resident getResident(){return null;}
    public String getLastLogin(){ return null; }

    public void setUsername(String username) { this.username = username; }
    public void setPassword(String password) { this.password = password; }


}
