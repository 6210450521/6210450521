package services;

import models.Room;
import models.RoomList;

import java.io.*;

public class RoomFileDataSource {
    private RoomList roomList;

    public RoomFileDataSource(){
        roomList= new RoomList();
    }

    public void checkFile(){
        roomList = new RoomList();
        File file = new File("Data");
        if (!file.exists()) {
            file.mkdirs();
        }

        checkAndReadRoomFile();

    }
    public void checkAndReadRoomFile(){
        String filePath = "Data"+ File.separator + "Room.csv";
        File file = new File(filePath);
        if (!file.exists()) {
            try{
                file.createNewFile();
            }catch(IOException e){
                System.out.println(e.getMessage());
            }
        }
        try{
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);
            String text = "";
            while((text = reader.readLine()) != null){
                String[] array = text.split(",");
                String roomNumber = array[0];
                String roomType = array[1];
                String[] residents = array[2].split("/");
                Room room = new Room(roomNumber,roomType);

                if (!residents[0].equals("Empty")){
                    for (int i=0; i<residents.length; i++){
                        room.addResident(residents[i]);
                    }
                }
                roomList.addRoom(room);
            }
            reader.close();
            fileReader.close();
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }
    public void newRoom(Room room ){
        if (!roomList.checkAlreadyHaveRoom(room.getRoomNumber())){
            roomList.addRoom(new Room(room.getRoomNumber(),room.getRoomType()));
        }
        try{
            FileWriter fileWriter = new FileWriter("Data"+ File.separator + "Room.csv",true);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            writer.append(room.getRoomNumber() + "," + room.getRoomType()+","+"Empty");
            writer.newLine();
            writer.close();
            fileWriter.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }


    public RoomList getRoomList() {
        return roomList;
    }
    public void addResident(String roomNum,String name){
        Room room = null;
        for (Room r : roomList.getRooms()){
            if (r.getRoomNumber().equals(roomNum)){
                room = r;
            }
        }

        room.addResident(name);
        try{
            FileWriter fileWriter = new FileWriter("Data"+ File.separator + "Room.csv");
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for(int i=0; i < roomList.getRooms().size();i++){
                String roomNumber = roomList.getRooms().get(i).getRoomNumber();
                String roomType = roomList.getRooms().get(i).getRoomType();
                String residentToString = roomList.getRooms().get(i).residentsToString();
                if (roomList.getRooms().get(i).getResidents().size() == 0){
                    residentToString = "Empty";
                }
                writer.append(roomNumber + "," + roomType + "," + residentToString);
                writer.newLine();
            }
            writer.close();
            fileWriter.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
