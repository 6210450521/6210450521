package models;

public class Document extends Mail {
    private String priority;
    public Document(Resident resident, String postman, String width, String height, String image,String status, String priority) {
        super(resident, postman, width, height, image, status);
        this.priority = priority;
    }
    public String getPriority() { return priority; }
    public String getType(){
        return "Document";
    }
}
