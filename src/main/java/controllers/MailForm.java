package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import models.Document;
import models.Mail;
import models.Parcel;
import models.Resident;
import services.AccountFileDataSource;
import services.MailFileDataSource;
import services.RoomFileDataSource;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;

public class MailForm {
    @FXML ComboBox<String> mailType,priority;
    @FXML TextField name,lastName,roomNumber,postman,width,height,company,trackingNumber;
    @FXML Label imageStatus;
    @FXML Button back,enter,uploadImage;
    @FXML ImageView background;
    private AccountFileDataSource accountFileDataSource;
    private RoomFileDataSource roomFileDataSource;
    private MailFileDataSource mailFileDataSource;
    private File staffImage;
    private String imagePath;
    private File selectedFile;

    public void setAccountFileDataSource(AccountFileDataSource accountFileDataSource) {
        this.accountFileDataSource = accountFileDataSource;
    }
    public void setRoomFileDataSource(RoomFileDataSource roomFileDataSource) {
        this.roomFileDataSource = roomFileDataSource;
    }
    public void setMailFileDataSource(MailFileDataSource mailFileDataSource) {
        this.mailFileDataSource = mailFileDataSource;
    }

    @FXML public void initialize(){
        background.setImage(new Image("/images/sharon-mccutcheon.jpg"));
        mailType.getItems().addAll("Mail","Document","Parcel");
        priority.getItems().addAll("Normal","Express");
        name.setDisable(true);
        lastName.setDisable(true);
        roomNumber.setDisable(true);
        postman.setDisable(true);
        width.setDisable(true);
        height.setDisable(true);
        company.setDisable(true);
        trackingNumber.setDisable(true);
        priority.setDisable(true);
    }
    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manage_mail.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        ManageMail controller = loader.getController();
        controller.setAccountFileDataSource(accountFileDataSource);
        controller.setRoomFileDataSource(roomFileDataSource);
        controller.setMailFileDataSource(mailFileDataSource);

        stage.show();
    }
    @FXML public void selectedMailType(){
        if (mailType.getSelectionModel().getSelectedItem().equals("Mail")) {
            name.setDisable(false);
            lastName.setDisable(false);
            roomNumber.setDisable(false);
            postman.setDisable(false);
            width.setDisable(false);
            height.setDisable(false);
            company.setDisable(true);
            trackingNumber.setDisable(true);
            priority.setDisable(true);
        }else if(mailType.getSelectionModel().getSelectedItem().equals("Document")){
            name.setDisable(false);
            lastName.setDisable(false);
            roomNumber.setDisable(false);
            postman.setDisable(false);
            width.setDisable(false);
            height.setDisable(false);
            priority.setDisable(false);
            company.setDisable(true);
            trackingNumber.setDisable(true);
        }else if(mailType.getSelectionModel().getSelectedItem().equals("Parcel")){
            name.setDisable(false);
            lastName.setDisable(false);
            roomNumber.setDisable(false);
            postman.setDisable(false);
            width.setDisable(false);
            height.setDisable(false);
            company.setDisable(false);
            trackingNumber.setDisable(false);
            priority.setDisable(true);
        }
    }
    @FXML public void handleEnterOnAction(ActionEvent event) throws IOException{
        String status = "Not received";


        if (mailType.getSelectionModel().getSelectedItem()==null||name.getText().equals("")||lastName.getText().equals("")||roomNumber.getText().equals("")||postman.getText().equals("")||width.getText().equals("")||height.getText().equals("")){
            {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Error!!");
                alert.setHeaderText(null);
                alert.setContentText("Please check your information and try again.");

                alert.showAndWait();
            }
        } else if (mailType.getSelectionModel().getSelectedItem().equals("Parcel" ) && (company.getText().equals("") || trackingNumber.getText().equals(""))){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error!!");
            alert.setHeaderText(null);
            alert.setContentText("Please check your information and try again.");
            alert.showAndWait();
        }else if (mailType.getSelectionModel().getSelectedItem().equals("Document")&& priority.getSelectionModel().getSelectedItem() == null){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error!!");
            alert.setHeaderText(null);
            alert.setContentText("Please check your information and try again.");
            alert.showAndWait();
        } else if (roomFileDataSource.getRoomList().checkRoom(roomNumber.getText())){
            try{
                if (imagePath != null) {
                    File dir = new File(imagePath);
                    Files.copy(staffImage.toPath(), dir.toPath(), StandardCopyOption.COPY_ATTRIBUTES);
                }
                String fileName;
                if(selectedFile != null){
                    fileName = selectedFile.getName();
                }else{
                    fileName = "-";
                }
                if(mailType.getSelectionModel().getSelectedItem().equals("Mail")){
                    Mail mail = new Mail(new Resident(name.getText(),lastName.getText(),roomNumber.getText())
                            ,postman.getText(), width.getText(),height.getText(), fileName,status);
                    mail.setStaffReceiver(accountFileDataSource.getAccountList().getCurrentAccount().getName());
                    mailFileDataSource.newMail(mail);
                }else if(mailType.getSelectionModel().getSelectedItem().equals("Document")){
                    Document document = new Document(new Resident(name.getText(),lastName.getText(),roomNumber.getText())
                            ,postman.getText(), width.getText(),height.getText(), fileName,status,priority.getSelectionModel().getSelectedItem());
                    document.setStaffReceiver(accountFileDataSource.getAccountList().getCurrentAccount().getName());
                    mailFileDataSource.newMail(document);
                }else if(mailType.getSelectionModel().getSelectedItem().equals("Parcel")){
                    Parcel parcel = new Parcel(new Resident(name.getText(),lastName.getText(),roomNumber.getText())
                            ,postman.getText(), width.getText(),height.getText(), fileName,status,company.getText(),trackingNumber.getText());
                    parcel.setStaffReceiver(accountFileDataSource.getAccountList().getCurrentAccount().getName());
                    mailFileDataSource.newMail(parcel);
                }
                Button e = (Button) event.getSource();
                Stage stage = (Stage) e.getScene().getWindow();

                FXMLLoader loader = new FXMLLoader(getClass().getResource("/mail_form.fxml"));
                stage.setScene(new Scene(loader.load(),1280,800));

                MailForm controller = loader.getController();
                controller.setAccountFileDataSource(accountFileDataSource);
                controller.setRoomFileDataSource(roomFileDataSource);
                controller.setMailFileDataSource(mailFileDataSource);

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Success!!");
                alert.setHeaderText(null);
                alert.setContentText("Add mail complete.");
                alert.showAndWait();

                stage.show();
            }catch (FileAlreadyExistsException e){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Error!!");
                alert.setHeaderText(null);
                alert.setContentText("File already exists.");
                alert.showAndWait();
            }

        }else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error!!");
            alert.setHeaderText(null);
            alert.setContentText("Room not found.");

            alert.showAndWait();
        }

    }
    @FXML public void handleUploadImageOnAction(ActionEvent event) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select image");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );
        selectedFile = fileChooser.showOpenDialog(null);
        if (selectedFile != null){
            imagePath = "src" + File.separator + "main" + File.separator + "resources" + File.separator + "mailsImage" + File.separator + selectedFile.getName();
            staffImage = selectedFile;
            imageStatus.setText(selectedFile.getName());
        }
    }

}
