package controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import models.Room;
import services.AccountFileDataSource;
import services.MailFileDataSource;
import services.RoomFileDataSource;
import services.StringConfiguration;

import java.io.IOException;
import java.util.ArrayList;

public class ListOfRooms {
    @FXML TableView<Room> roomTable;
    @FXML Label resident1, resident2, resident3,  resident4;
    @FXML TextField search;
    @FXML Button back,residents;
    @FXML ImageView background;
    private AccountFileDataSource accountFileDataSource;
    private RoomFileDataSource roomFileDataSource;
    private MailFileDataSource mailFileDataSource;
    private Room selectedRoom;
    private ObservableList<Room> roomObservableList;

    public void setAccountFileDataSource(AccountFileDataSource accountFileDataSource) {
        this.accountFileDataSource = accountFileDataSource;
    }
    public void setRoomFileDataSource(RoomFileDataSource roomFileDataSource){
        this.roomFileDataSource = roomFileDataSource;
    }
    public void setMailFileDataSource(MailFileDataSource mailFileDataSource) {
        this.mailFileDataSource = mailFileDataSource;
    }

    @FXML public void initialize(){
        background.setImage(new Image("/images/maarten-deckers.jpg"));
    }
    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manage_resident.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        ManageResident controller = loader.getController();
        controller.setAccountFileDataSource(accountFileDataSource);
        controller.setRoomFileDataSource(roomFileDataSource);
        controller.setMailFileDataSource(mailFileDataSource);

        stage.show();
    }
    public void showRoomData(){
        roomObservableList = FXCollections.observableArrayList(roomFileDataSource.getRoomList().getRooms());
        roomTable.setItems(roomObservableList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Number", "field:roomNumber"));
        configs.add(new StringConfiguration("title:Type", "field:roomType"));
        configs.add(new StringConfiguration("title:Floor", "field:floor"));
        configs.add(new StringConfiguration("title:Building", "field:building"));

        for (StringConfiguration conf: configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            roomTable.getColumns().add(col);
        }
    }
    public void setTable(){
        showRoomData();

        roomTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            resident1.setText("");
            resident2.setText("");
            resident3.setText("");
            resident4.setText("");
            if (newValue != null) {
                showSelectedRoom( newValue);
                if (selectedRoom.getResidents().size() >= 1){
                    resident1.setText(selectedRoom.getResidents().get(0));
                }
                if (selectedRoom.getResidents().size() >= 2){
                    resident2.setText(selectedRoom.getResidents().get(1));
                }
                if (selectedRoom.getResidents().size() >= 3){
                    resident3.setText(selectedRoom.getResidents().get(2));
                }
                if (selectedRoom.getResidents().size() >= 4){
                    resident4.setText(selectedRoom.getResidents().get(3));
                }
            }
        });
        searchRoomByName();

    }

    public void showSelectedRoom(Room room) {
        selectedRoom = room;
    }

    public void searchRoomByName(){
        FilteredList<Room> filteredData = new FilteredList<>(roomObservableList, p -> true);

        search.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(room -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String filter = newValue;

                for (String resident : room.getResidents()){
                    if (resident.toLowerCase().contains(filter.toLowerCase())) {
                        return true;
                    }
                }
                return false;
            });
        });

        SortedList<Room> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(roomTable.comparatorProperty());

        roomTable.setItems(sortedData);
    }
}
