package models;

import java.util.ArrayList;

public class Room {
    private String roomNumber;
    private String roomType;
    private String floor;
    private String building;

    private ArrayList<String> residents;

    public Room(String roomNumber, String roomType) {
        this.roomNumber = roomNumber;
        this.roomType = roomType;
        this.floor = String.valueOf(roomNumber.charAt(1));
        this.building = String.valueOf(roomNumber.charAt(0));
        residents = new ArrayList<>();
    }

    public String getRoomType() {
        return roomType;
    }
    public String getRoomNumber() {
        return roomNumber;
    }
    public String getFloor() { return floor; }
    public String getBuilding() { return building; }
    public ArrayList<String> getResidents() { return residents; }

    public String residentsToString(){
        String str = "";
        for (int i=0; i<residents.size(); i++){
            if (i == 0){
                str += residents.get(i);
            }else {
                str += "/" + residents.get(i);
            }
        }
        return str;
    }

    public void addResident(String name){
            residents.add(name);
    }

    public boolean checkResident(String resident){
        for (String str : residents){
            if (str.equals(resident)){
                return true;
            }
        }
        return false;
    }
}
