package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import models.Resident;
import models.UserAccount;
import services.AccountFileDataSource;
import services.RoomFileDataSource;

import java.io.*;

public class UserRegistration {
    @FXML TextField name,lastName,username;
    @FXML TextField room;
    @FXML PasswordField password, confirmPassword;
    @FXML Button enter, back;
    @FXML ImageView background;
    private AccountFileDataSource accountFileDataSource;
    private RoomFileDataSource roomFileDataSource;

    public void setAccountFileDataSource(AccountFileDataSource accountFileDataSource) {
        this.accountFileDataSource = accountFileDataSource;
    }
    public void setRoomFileDataSource(RoomFileDataSource roomFileDataSource) {
        this.roomFileDataSource = roomFileDataSource;
    }

    @FXML public void initialize(){
        background.setImage(new Image("/images/liana-mikah.jpg"));
    }
    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        stage.show();
    }
    @FXML public void handleEnterOnAction(ActionEvent event)throws IOException{

        if (!password.getText().equals(confirmPassword.getText()) || (username.getText().equals("") || name.getText().equals("") ||
                lastName.getText().equals("") || password.getText().equals("") || confirmPassword.getText().equals("") || room.getText().equals(""))){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error!!");
            alert.setHeaderText(null);
            alert.setContentText("Please check your information and try again.");

            alert.showAndWait();
        } else if (accountFileDataSource.getAccountList().checkAccountAlreadyUse(username.getText())){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error!!");
            alert.setHeaderText(null);
            alert.setContentText("This username already exists.");

            alert.showAndWait();
        }else if (!roomFileDataSource.getRoomList().checkResidentRoom(room.getText(),name.getText(),lastName.getText())){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Room Error!!");
            alert.setHeaderText(null);
            alert.setContentText("Please check your information and try again.");

            alert.showAndWait();
        }else{
            Button e = (Button) event.getSource();
            Stage stage = (Stage) e.getScene().getWindow();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/user_registration.fxml"));
            stage.setScene(new Scene(loader.load(),1280,800));

            if(password.getText().equals(confirmPassword.getText())){
                accountFileDataSource.newUserAccount(new UserAccount(new Resident(name.getText(),lastName.getText(),room.getText()),username.getText(), password.getText()));
            }

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Success!!");
            alert.setHeaderText(null);
            alert.setContentText("Registration completed successfully.");

            alert.showAndWait();
            stage.show();
        }
    }
}