package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;

public class Recommend {
    @FXML Button back;
    @FXML ImageView background;

    @FXML public void initialize() throws IOException {
        background.setImage(new Image("/images/cookies pastel.jpg"));
    }
    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/menu.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        stage.show();
    }
}
