package models;

public class Resident {
    private String name;
    private String lastName;
    private String roomNumber;

    public Resident(String name, String lastName, String roomNumber){
        this.name = name;
        this.lastName = lastName;
        this.roomNumber = roomNumber;
    }

    public String getName() {
        return name;
    }
    public String getLastName() { return lastName; }
    public String getRoomNumber() { return roomNumber; }
}
