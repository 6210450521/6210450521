package services;

import models.*;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MailFileDataSource {
    private MailList mailList;

    public MailFileDataSource(){
        mailList = new MailList();
    }

    public void checkFile(){
        mailList = new MailList();
        File file = new File("Data");
        if (!file.exists()) {
            file.mkdirs();
        }

        checkAndReadMailFile();
    }
    public void checkAndReadMailFile(){
        String filePath = "Data"+ File.separator + "Mail.csv";
        File file = new File(filePath);
        if (!file.exists()) {
            try{
                file.createNewFile();
            }catch(IOException e){
                System.out.println(e.getMessage());
            }
        }
        try{
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);
            String text = "";
            while((text = reader.readLine()) != null){
                String[] array = text.split(",");
                String type = array[0];
                String name = array[1];
                String lastName = array[2];
                String roomNumber = array[3];
                String postman = array[4];
                String width = array[5];
                String height = array[6];
                String image = array[7];
                String status = array[8];
                String residentReceiver = array[9];
                if (type.equals("Mail")){
                    Mail mail = new Mail(new Resident(name,lastName,roomNumber), postman, width, height, image, status);
                    mail.setResidentReceiver(residentReceiver);
                    String dateTimeMail = array[10];
                    String staffReceiver = array[11];
                    String receiveDateTime = array[12];
                    String receiveFromStaff = array[13];
                    if (!dateTimeMail.equals("-")){
                        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
                        LocalDateTime dateTime = LocalDateTime.parse(dateTimeMail,dateTimeFormatter);
                        mail.setSendDateTime(dateTime);
                    }
                    if (!receiveDateTime.equals("-")){
                        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
                        LocalDateTime dateTime = LocalDateTime.parse(receiveDateTime,dateTimeFormatter);
                        mail.setReceiveDateTime(dateTime);
                    }
                    mail.setStaffReceiver(staffReceiver);
                    mail.setResidentReceiver(residentReceiver);
                    mail.setReceiveFromStaff(receiveFromStaff);
                    mailList.addMail(mail);
                }else if (type.equals("Document")){
                    String priority = array[10];
                    Document document = new Document(new Resident(name,lastName,roomNumber), postman, width, height, image, status, priority);
                    String dateTimeMail = array[11];
                    String staffReceiver = array[12];
                    String receiveDateTime = array[13];
                    String receiveFromStaff = array[14];
                    if (!dateTimeMail.equals("-")){
                        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
                        LocalDateTime dateTime = LocalDateTime.parse(dateTimeMail,dateTimeFormatter);
                        document.setSendDateTime(dateTime);
                    }
                    if (!receiveDateTime.equals("-")){
                        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
                        LocalDateTime dateTime = LocalDateTime.parse(receiveDateTime,dateTimeFormatter);
                        document.setReceiveDateTime(dateTime);
                    }
                    document.setStaffReceiver(staffReceiver);
                    document.setResidentReceiver(residentReceiver);
                    document.setReceiveFromStaff(receiveFromStaff);
                    mailList.addMail(document);
                }else if (type.equals("Parcel")){
                    String company = array[10];
                    String trackingNumber = array[11];
                    Parcel parcel = new Parcel(new Resident(name,lastName,roomNumber), postman, width, height, image, status, company, trackingNumber);
                    String dateTimeMail = array[12];
                    String staffReceiver = array[13];
                    String receiveDateTime = array[14];
                    String receiveFromStaff = array[15];
                    if (!dateTimeMail.equals("-")){
                        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
                        LocalDateTime dateTime = LocalDateTime.parse(dateTimeMail,dateTimeFormatter);
                        parcel.setSendDateTime(dateTime);
                    }
                    if (!receiveDateTime.equals("-")){
                        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
                        LocalDateTime dateTime = LocalDateTime.parse(receiveDateTime,dateTimeFormatter);
                        parcel.setReceiveDateTime(dateTime);
                    }
                    parcel.setStaffReceiver(staffReceiver);
                    parcel.setResidentReceiver(residentReceiver);
                    parcel.setReceiveFromStaff(receiveFromStaff);
                    mailList.addMail(parcel);
                }
            }
            reader.close();
            fileReader.close();
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }
    public void newMail(Mail mail){
        mail.setSendDateTime(LocalDateTime.now());
        mailList.addMail(mail);

        try{
            FileWriter fileWriter = new FileWriter("Data"+ File.separator + "Mail.csv",true);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            String type = mail.getType();
            String name = mail.getName();
            String lastName = mail.getLastName();
            String roomNumber = mail.getRoomNumber();
            String postman = mail.getPostman();
            String width = mail.getWidth();
            String height = mail.getHeight();
            String image;
            String priority = mail.getPriority();
            String company = mail.getCompany();
            String trackingNumber = mail.getTrackingNumber();
            String dateTime = mail.getSendDateTime();
            String status = mail.getStatus();
            String staffReceiver = mail.getStaffReceiver();
            String residentReceiver = "Empty";
            String receiveDateTime = "-";
            String receiveFromStaff = "Empty";
            if (mail.getImage() == null){
                image = "-";
            }else{
                image = mail.getImage();
            }
            if (type.equals("Mail")){
                writer.append(type+","+name+","+lastName+","+roomNumber+","+postman+","+width+","+height+","+image+","+ status+","+ residentReceiver+","+dateTime + ","+ staffReceiver +","+receiveDateTime + "," + receiveFromStaff);
            }else if(type.equals("Document")){
                writer.append(type+","+name+","+lastName+","+roomNumber+","+postman+","+width+","+height+","+image+","+ status+","+ residentReceiver+","+priority+","+dateTime+ ","+ staffReceiver+","+receiveDateTime+ "," + receiveFromStaff);
            }else if(type.equals("Parcel")){
                writer.append(type+","+name+","+lastName+","+roomNumber+","+postman+","+width+","+height+","+image+","+ status+","+ residentReceiver+","+company+","+trackingNumber+","+dateTime+ ","+ staffReceiver+","+receiveDateTime+ "," + receiveFromStaff);
            }
            writer.newLine();
            writer.close();
            fileWriter.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }


    public MailList getMailList() {
        return mailList;
    }
    public void receiveMail(Mail mail,String receiverName){
        for (Mail m : mailList.getMails()){
            if (m.equals(mail)){
                m.setStatus("Received");
                m.setResidentReceiver(receiverName);
            }
        }
        try{
            FileWriter fileWriter = new FileWriter("Data"+ File.separator + "Mail.csv");
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for(int i=0; i<mailList.getMails().size(); i++){
                String type = mailList.getMails().get(i).getType();
                String name = mailList.getMails().get(i).getName();
                String lastName = mailList.getMails().get(i).getLastName();
                String roomNumber = mailList.getMails().get(i).getRoomNumber();
                String postman = mailList.getMails().get(i).getPostman();
                String width = mailList.getMails().get(i).getWidth();
                String height = mailList.getMails().get(i).getHeight();
                String picture = mailList.getMails().get(i).getImage();
                String priority = mailList.getMails().get(i).getPriority();
                String company = mailList.getMails().get(i).getCompany();
                String trackingNumber = mailList.getMails().get(i).getTrackingNumber();
                String dateTime = mailList.getMails().get(i).getSendDateTime();
                String status = mailList.getMails().get(i).getStatus();
                String residentReceiver = mailList.getMails().get(i).getResidentReceiver();
                String staffReceiver = mailList.getMails().get(i).getStaffReceiver();
                String receiveDateTime = mailList.getMails().get(i).getReceiveDateTime();
                String receiveFromStaff = mailList.getMails().get(i).getReceiveFromStaff();
                if (type.equals("Mail")){
                    writer.append(type+","+name+","+lastName+","+roomNumber+","+postman+","+width+","+height+","+picture+","+status+","+ residentReceiver +","+dateTime + ","+ staffReceiver +","+receiveDateTime + "," + receiveFromStaff);
                }else if(type.equals("Document")){
                    writer.append(type+","+name+","+lastName+","+roomNumber+","+postman+","+width+","+height+","+picture+","+status+","+ residentReceiver + ","+priority+","+dateTime+","+ staffReceiver +","+receiveDateTime+ "," + receiveFromStaff);
                }else if(type.equals("Parcel")){
                    writer.append(type+","+name+","+lastName+","+roomNumber+","+postman+","+width+","+height+","+picture+","+status+","+ residentReceiver +","+company+","+trackingNumber+","+dateTime+","+ staffReceiver +","+receiveDateTime+ "," + receiveFromStaff);
                }
                writer.newLine();
            }
            writer.close();
            fileWriter.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
