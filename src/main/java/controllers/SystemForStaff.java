package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import services.AccountFileDataSource;
import services.MailFileDataSource;
import services.RoomFileDataSource;

import java.io.IOException;

public class SystemForStaff {
    @FXML Button manageResident, manageMail, changeStaffPassword, back;
    @FXML ImageView background;
    private AccountFileDataSource accountFileDataSource;
    private RoomFileDataSource roomFileDataSource;
    private MailFileDataSource mailFileDataSource;

    public void setAccountFileDataSource(AccountFileDataSource account) {
        this.accountFileDataSource = account;
    }
    public void setRoomFileDataSource(RoomFileDataSource room){
        this.roomFileDataSource = room;
    }
    public void setMailFileDataSource(MailFileDataSource mailFileDataSource) {
        this.mailFileDataSource = mailFileDataSource;
    }

    @FXML public void initialize(){
        background.setImage(new Image("/images/lum.jpg"));
    }
    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        stage.show();
    }
    @FXML public void handleManageResidentOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manage_resident.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        ManageResident controller = loader.getController();
        controller.setAccountFileDataSource(accountFileDataSource);
        controller.setRoomFileDataSource(roomFileDataSource);
        controller.setMailFileDataSource(mailFileDataSource);

        stage.show();
    }
    @FXML public void handleManageMailOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manage_mail.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        ManageMail controller = loader.getController();
        controller.setAccountFileDataSource(accountFileDataSource);
        controller.setRoomFileDataSource(roomFileDataSource);
        controller.setMailFileDataSource(mailFileDataSource);

        stage.show();
    }
    @FXML public void handleChangeStaffPasswordOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/change_staff_password.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        ChangeStaffPassword controller = loader.getController();
        controller.setAccountFileDataSource(accountFileDataSource);
        controller.setRoomFileDataSource(roomFileDataSource);
        controller.setMailFileDataSource(mailFileDataSource);

        stage.show();
    }
}
