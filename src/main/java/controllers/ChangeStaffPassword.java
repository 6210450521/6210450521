package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import models.StaffAccount;
import services.AccountFileDataSource;
import services.MailFileDataSource;
import services.RoomFileDataSource;

import java.io.IOException;

public class ChangeStaffPassword {
    @FXML PasswordField oldPassword,newPassword, confirmPassword;
    @FXML Button enter, back;
    @FXML ImageView background;
    private AccountFileDataSource accountFileDataSource;
    private RoomFileDataSource roomFileDataSource;
    private MailFileDataSource mailFileDataSource;

    public void setAccountFileDataSource(AccountFileDataSource accountFileDataSource) {
        this.accountFileDataSource = accountFileDataSource;
    }
    public void setRoomFileDataSource(RoomFileDataSource roomFileDataSource) {
        this.roomFileDataSource = roomFileDataSource;
    }
    public void setMailFileDataSource(MailFileDataSource mailFileDataSource) {
        this.mailFileDataSource = mailFileDataSource;
    }

    @FXML public void initialize(){
        background.setImage(new Image("/images/nordwood.jpg"));
    }
    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/system_for_staff.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        SystemForStaff controller = loader.getController();
        controller.setAccountFileDataSource(accountFileDataSource);
        controller.setRoomFileDataSource(roomFileDataSource);
        controller.setMailFileDataSource(mailFileDataSource);

        stage.show();
    }
    @FXML public void handleEnterOnAction(ActionEvent event) throws IOException {
        if (!newPassword.getText().equals(confirmPassword.getText()) || oldPassword.getText().equals("") || newPassword.getText().equals("") || confirmPassword.getText().equals("") ||
                oldPassword.getText().equals(newPassword.getText()) || !oldPassword.getText().equals(accountFileDataSource.getAccountList().getCurrentAccount().getPassword())){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error!!");
            alert.setHeaderText(null);
            alert.setContentText("Please check your information and try again.");

            alert.showAndWait();
        }else{
            Button e = (Button) event.getSource();
            Stage stage = (Stage) e.getScene().getWindow();

            accountFileDataSource.changeStaffPassword(new StaffAccount(accountFileDataSource.getAccountList().getCurrentAccount().getUsername()
                    ,newPassword.getText(), accountFileDataSource.getAccountList().getCurrentAccount().getName()
                    , accountFileDataSource.getAccountList().getCurrentAccount().getLastName()));

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/change_staff_password.fxml"));
            stage.setScene(new Scene(loader.load(),1280,800));

            ChangeStaffPassword controller = loader.getController();
            controller.setAccountFileDataSource(accountFileDataSource);
            controller.setRoomFileDataSource(roomFileDataSource);
            controller.setMailFileDataSource(mailFileDataSource);

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Success!!");
            alert.setHeaderText(null);
            alert.setContentText("Change password complete.");
            alert.showAndWait();

            stage.show();

        }
    }
}
