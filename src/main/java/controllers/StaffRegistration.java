package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import models.StaffAccount;
import services.AccountFileDataSource;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class StaffRegistration {
    @FXML TextField name;
    @FXML TextField lastName;
    @FXML TextField username;
    @FXML PasswordField password, confirmPassword;
    @FXML Button enter, back, uploadImage;
    @FXML ImageView background;
    @FXML Label imageStatus;
    private AccountFileDataSource accountFileDataSource;
    private File staffImage;
    private String imagePath;
    private File selectedFile;

    public void setAccountFileDataSource(AccountFileDataSource accountFileDataSource) {
        this.accountFileDataSource = accountFileDataSource;
    }

    @FXML public void initialize(){
        background.setImage(new Image("/images/liana-mikah-2.jpg"));
    }
    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/system_for_admin.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        SystemForAdmin Controller = loader.getController();
        Controller.setAccountFileDataSource(accountFileDataSource);

        stage.show();
    }
    @FXML public void handleEnterOnAction(ActionEvent event)throws IOException {
        if ((!password.getText().equals(confirmPassword.getText())) || (username.getText().equals("") || name.getText().equals("") ||
                lastName.getText().equals("") || password.getText().equals("") || confirmPassword.getText().equals("") )){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error!!");
            alert.setHeaderText(null);
            alert.setContentText("Please check your information and try again.");

            alert.showAndWait();
        } else if (accountFileDataSource.getAccountList().checkAccountAlreadyUse(username.getText())){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error!!");
            alert.setHeaderText(null);
            alert.setContentText("This username already exists.");

            alert.showAndWait();
        }else{
            try{
                StaffAccount staffAccount = new StaffAccount(username.getText(), password.getText(),name.getText(),lastName.getText());
                if (imagePath != null) {
                    File dir = new File(imagePath);
                    Files.copy(staffImage.toPath(), dir.toPath(), StandardCopyOption.COPY_ATTRIBUTES);
                    staffAccount.setImage(selectedFile.getName());
                }
                accountFileDataSource.newStaffAccount(staffAccount);

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Success!!");
                alert.setHeaderText(null);
                alert.setContentText("Registration completed successfully.");
                alert.showAndWait();

                Button e = (Button) event.getSource();
                Stage stage = (Stage) e.getScene().getWindow();

                FXMLLoader loader = new FXMLLoader(getClass().getResource("/staff_registration.fxml"));
                stage.setScene(new Scene(loader.load(),1280,800));

                StaffRegistration Controller = loader.getController();
                Controller.setAccountFileDataSource(accountFileDataSource);

                stage.show();
            }catch(FileAlreadyExistsException e){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Error!!");
                alert.setHeaderText(null);
                alert.setContentText("File already exists.");
                alert.showAndWait();
            }
        }
    }
    @FXML public void handleUploadImageOnAction(ActionEvent event) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select image");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );
        selectedFile = fileChooser.showOpenDialog(null);
        if (selectedFile != null){
            imagePath = "src" + File.separator + "main" + File.separator + "resources" + File.separator + "staffsImage" + File.separator + selectedFile.getName();
            staffImage = selectedFile;

        }
    }
}
