package models;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class StaffAccount extends Account{
    private String name;
    private String lastName;
    private LocalDateTime lastLogin;
    private String status;
    private int bannedLogin;
    private String image;


    public StaffAccount(String username, String password,String name,String lastName) {
        super(username, password);
        this.name = name;
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }
    public String getLastName() {
        return lastName;
    }
    public String getLastLogin() {
        if (lastLogin == null){
            return "-";
        }
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
        String formattedDateTime = lastLogin.format(dateTimeFormatter);
        return formattedDateTime;
    }

    public String getStatus() { return status; }

    public int getBannedLogin() {
        return bannedLogin;
    }
    public String getImage() { return image; }

    public void setName(String name) { this.name = name; }
    public void setLastName(String lastName) { this.lastName = lastName; }
    public void setLastLogin(LocalDateTime lastLogin) { this.lastLogin = lastLogin; }
    public void setImage(String image) { this.image = image; }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setBannedLogin(int bannedLogin) {
        this.bannedLogin = bannedLogin;
    }
    public void bannedLogin(){ bannedLogin += 1; }


}
