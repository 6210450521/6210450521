package controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import models.StaffAccount;
import services.AccountFileDataSource;
import services.StringConfiguration;

import java.io.IOException;
import java.util.ArrayList;

public class ListOfStaffs {
    @FXML TableView<StaffAccount> table;
    @FXML Button back,ban;
    @FXML ImageView background, staffImage;
    private AccountFileDataSource accountFileDataSource;
    private StaffAccount selectedStaffAccount;
    private ObservableList<StaffAccount> staffAccountObservableList;

    public void setAccountFileDataSource(AccountFileDataSource accountFileDataSource) { this.accountFileDataSource = accountFileDataSource; }

    @FXML public void initialize(){
        background.setImage(new Image("/images/keila-hotzel-2.jpg"));
        ban.setDisable(true);
    }
    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/system_for_admin.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        SystemForAdmin controller = loader.getController();
        controller.setAccountFileDataSource(accountFileDataSource);

        stage.show();
    }

    @FXML public void handleBanOnAction(ActionEvent event) throws IOException {
        if (selectedStaffAccount.getStatus().equals("Not Banned")){
            for (StaffAccount s: accountFileDataSource.getAccountList().getStaffAccounts()){
                if (selectedStaffAccount.equals(s)){
                    s.setStatus("Banned");
                }
            }
        }else{
            for (StaffAccount s: accountFileDataSource.getAccountList().getStaffAccounts()){
                if (selectedStaffAccount.equals(s)){
                    s.setStatus("Not Banned");
                }
            }
        }
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/list_of_staffs.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        accountFileDataSource.updateStaff();
        ListOfStaffs controller = loader.getController();
        controller.setAccountFileDataSource(accountFileDataSource);
        controller.setTable();

        stage.show();

    }
    public void showStaffData(){
        staffAccountObservableList = FXCollections.observableArrayList(accountFileDataSource.getAccountList().getStaffAccounts());
        table.setItems(staffAccountObservableList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Name", "field:name"));
        configs.add(new StringConfiguration("title:Last Name", "field:lastName"));
        configs.add(new StringConfiguration("title:Username", "field:username"));
        configs.add(new StringConfiguration("title:Last login", "field:lastLogin"));
        configs.add(new StringConfiguration("title:Status", "field:status"));
        configs.add(new StringConfiguration("title:Banned Login", "field:bannedLogin"));

        TableColumn nameCol = new TableColumn(configs.get(0).get("title"));
        nameCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(0).get("field")));
        table.getColumns().add(nameCol);
        TableColumn lastNameCol = new TableColumn(configs.get(1).get("title"));
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(1).get("field")));
        table.getColumns().add(lastNameCol);
        TableColumn usernameCol = new TableColumn(configs.get(2).get("title"));
        usernameCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(2).get("field")));
        table.getColumns().add(usernameCol);
        TableColumn dateTimeCol = new TableColumn(configs.get(3).get("title"));
        dateTimeCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(3).get("field")));
        table.getColumns().add(dateTimeCol);
        TableColumn statusCol = new TableColumn(configs.get(4).get("title"));
        statusCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(4).get("field")));
        table.getColumns().add(statusCol);
        TableColumn bannedLoginCol = new TableColumn(configs.get(5).get("title"));
        bannedLoginCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(5).get("field")));
        table.getColumns().add(bannedLoginCol);

        dateTimeCol.setSortType(TableColumn.SortType.DESCENDING);
        table.getSortOrder().setAll(dateTimeCol);
    }
    public void setTable(){
        showStaffData();
        table.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                showSelectedStaff(newValue);
                ban.setDisable(false);
                if (selectedStaffAccount.getStatus().equals("Banned")){
                    ban.setText("Unbanned");
                }else{
                    ban.setText("Ban");
                }
            }
        });
    }

    private void showSelectedStaff(StaffAccount staffAccount) {
        selectedStaffAccount = staffAccount;
        staffImage.setImage(null);
        if (!selectedStaffAccount.getImage().equals("-")){
            staffImage.setImage(new Image("/staffsImage/" + selectedStaffAccount.getImage()));
        }
    }
}
