package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import models.AdminAccount;

import services.AccountFileDataSource;

import java.io.IOException;

public class ChangeAdminPassword {
    @FXML PasswordField oldPassword,newPassword, confirmPassword;
    @FXML Button back;
    @FXML ImageView background;
    private AccountFileDataSource accountFileDataSource;

    public void setAccountFileDataSource(AccountFileDataSource accountFileDataSource) {
        this.accountFileDataSource = accountFileDataSource;
    }

    @FXML public void initialize(){
        background.setImage(new Image("/images/kristi-martin.jpg"));
    }
    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/system_for_admin.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        SystemForAdmin controller = loader.getController();
        controller.setAccountFileDataSource(accountFileDataSource);

        stage.show();
    }
    @FXML public void handleEnterOnAction(ActionEvent event) throws IOException {


        if (!newPassword.getText().equals(confirmPassword.getText()) || oldPassword.getText().equals("") || newPassword.getText().equals("") || confirmPassword.getText().equals("") ||
        oldPassword.getText().equals(newPassword.getText()) || !oldPassword.getText().equals(accountFileDataSource.getAccountList().getCurrentAccount().getPassword())){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error!!");
            alert.setHeaderText(null);
            alert.setContentText("Please check your information and try again.");

            alert.showAndWait();
        }else{
            Button e = (Button) event.getSource();
            Stage stage = (Stage) e.getScene().getWindow();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/change_admin_password.fxml"));
            stage.setScene(new Scene(loader.load(),1280,800));

            accountFileDataSource.changeAdminPassword(new AdminAccount(accountFileDataSource.getAccountList().getCurrentAccount().getUsername(),newPassword.getText()));
            ChangeAdminPassword controller = loader.getController();
            controller.setAccountFileDataSource(accountFileDataSource);

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Success!!");
            alert.setHeaderText(null);
            alert.setContentText("Change password complete.");
            alert.showAndWait();

            stage.show();
        }
    }
}
