package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import services.AccountFileDataSource;

import java.io.IOException;

public class SystemForAdmin {
    @FXML Button createStaff, listOfStaffs, changeStaffPassword, back;
    @FXML ImageView background;
    private AccountFileDataSource accountFileDataSource;

    public void setAccountFileDataSource(AccountFileDataSource account) {
        this.accountFileDataSource = account;
    }

    @FXML public void initialize(){
        background.setImage(new Image("/images/scott-webb.jpg"));
    }
    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        stage.show();
    }
    @FXML public void handleCreateStaffOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/staff_registration.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        StaffRegistration controller = loader.getController();
        controller.setAccountFileDataSource(accountFileDataSource);

        stage.show();
    }
    @FXML public void handleListOfStaffsOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/list_of_staffs.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        ListOfStaffs controller = loader.getController();
        controller.setAccountFileDataSource(accountFileDataSource);
        controller.setTable();

        stage.show();
    }
    @FXML public void handleChangeStaffPasswordOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/change_admin_password.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        ChangeAdminPassword controller = loader.getController();
        controller.setAccountFileDataSource(accountFileDataSource);

        stage.show();
    }
}
