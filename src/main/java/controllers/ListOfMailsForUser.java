package controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import models.Mail;
import services.AccountFileDataSource;
import services.MailFileDataSource;
import services.RoomFileDataSource;
import services.StringConfiguration;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

public class ListOfMailsForUser {
    @FXML TableView<Mail> table;
    @FXML ComboBox<String> sortBy,tableType;
    @FXML ImageView background, mailImage;
    @FXML Button back;
    @FXML Label staffReceiver,residentReceiver,receiveDateTime,receiveFrom;
    private AccountFileDataSource accountFileDataSource;
    private RoomFileDataSource roomFileDataSource;
    private MailFileDataSource mailFileDataSource;
    private Mail selectedMail;
    private ObservableList<Mail> mailObservableList;
    private ArrayList<Mail> userMail;
    private TableColumn dateTimeColumn,roomNumberColumn;

    public void setAccountFileDataSource(AccountFileDataSource accountFileDataSource) {
        this.accountFileDataSource = accountFileDataSource;
    }
    public void setRoomFileDataSource(RoomFileDataSource roomFileDataSource) {
        this.roomFileDataSource = roomFileDataSource;
    }
    public void setMailFileDataSource(MailFileDataSource mailFileDataSource) {
        this.mailFileDataSource = mailFileDataSource;
    }

    public void initialize(){
        background.setImage(new Image("/images/pawel-czerwinski.jpg"));
        userMail = new ArrayList<>();
        tableType.getItems().setAll("Mail","Document","Parcel");
    }
    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/system_for_user.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        SystemForUser controller = loader.getController();
        controller.setAccountFileDataSource(accountFileDataSource);
        controller.setRoomFileDataSource(roomFileDataSource);
        controller.setMailFileDataSource(mailFileDataSource);

        stage.show();
    }

    public void showMailData(String mailType){
        if(mailType == null || mailType.equals("Mail")){
            for(Mail m : mailFileDataSource.getMailList().getMails()){
                if(m.getName().equals(accountFileDataSource.getAccountList().getCurrentAccount().getResident().getName()) &&
                   m.getLastName().equals(accountFileDataSource.getAccountList().getCurrentAccount().getResident().getLastName()) && m.getType().equals("Mail")){
                    userMail.add(m);
                }
            }
            mailObservableList = FXCollections.observableArrayList(userMail);
            table.setItems(mailObservableList);

            ArrayList<StringConfiguration> configs = new ArrayList<>();
            configs.add(new StringConfiguration("title:Room number", "field:roomNumber"));
            configs.add(new StringConfiguration("title:Receiver name", "field:name"));
            configs.add(new StringConfiguration("title:Sender name", "field:postman"));
            configs.add(new StringConfiguration("title:Width", "field:width"));
            configs.add(new StringConfiguration("title:Height", "field:height"));
            configs.add(new StringConfiguration("title:Send date time", "field:sendDateTime"));
            configs.add(new StringConfiguration("title:Status", "field:status"));

            TableColumn roomNumberCol = new TableColumn(configs.get(0).get("title"));
            TableColumn receiverNameCol = new TableColumn(configs.get(1).get("title"));
            TableColumn senderNameCol = new TableColumn(configs.get(2).get("title"));
            TableColumn widthCol = new TableColumn(configs.get(3).get("title"));
            TableColumn heightCol = new TableColumn(configs.get(4).get("title"));
            TableColumn dateTimeCol = new TableColumn(configs.get(5).get("title"));
            TableColumn statusCol = new TableColumn(configs.get(6).get("title"));

            roomNumberCol.setPrefWidth(150);
            receiverNameCol.setPrefWidth(150);
            senderNameCol.setPrefWidth(150);
            widthCol.setPrefWidth(150);
            heightCol.setPrefWidth(150);
            heightCol.setPrefWidth(150);
            dateTimeCol.setPrefWidth(150);
            statusCol.setPrefWidth(150);

            roomNumberCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(0).get("field")));
            receiverNameCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(1).get("field")));
            senderNameCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(2).get("field")));
            widthCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(3).get("field")));
            heightCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(4).get("field")));
            dateTimeCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(5).get("field")));
            statusCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(6).get("field")));

            dateTimeColumn = dateTimeCol;
            roomNumberColumn = roomNumberCol ;

            table.getColumns().addAll(roomNumberCol,receiverNameCol,senderNameCol,widthCol,heightCol,dateTimeCol,statusCol);

            dateTimeCol.setSortType(TableColumn.SortType.DESCENDING);
            table.getSortOrder().setAll(dateTimeCol);
        }else if (mailType.equals("Document")){
            for(Mail m : mailFileDataSource.getMailList().getMails()){
                if(m.getName().equals(accountFileDataSource.getAccountList().getCurrentAccount().getResident().getName()) &&
                        m.getLastName().equals(accountFileDataSource.getAccountList().getCurrentAccount().getResident().getLastName())&& m.getType().equals("Document")){
                    userMail.add(m);
                }
            }
            mailObservableList = FXCollections.observableArrayList(userMail);
            table.setItems(mailObservableList);

            ArrayList<StringConfiguration> configs = new ArrayList<>();
            configs.add(new StringConfiguration("title:Room number", "field:roomNumber"));
            configs.add(new StringConfiguration("title:Receiver name", "field:name"));
            configs.add(new StringConfiguration("title:Sender name", "field:postman"));
            configs.add(new StringConfiguration("title:Width", "field:width"));
            configs.add(new StringConfiguration("title:Height", "field:height"));
            configs.add(new StringConfiguration("title:Priority", "field:priority"));
            configs.add(new StringConfiguration("title:Send date time", "field:sendDateTime"));
            configs.add(new StringConfiguration("title:Status", "field:status"));


            TableColumn roomNumberCol = new TableColumn(configs.get(0).get("title"));
            TableColumn receiveNameCol = new TableColumn(configs.get(1).get("title"));
            TableColumn senderNameCol = new TableColumn(configs.get(2).get("title"));
            TableColumn widthCol = new TableColumn(configs.get(3).get("title"));
            TableColumn heightCol = new TableColumn(configs.get(4).get("title"));
            TableColumn priorityCol = new TableColumn(configs.get(5).get("title"));
            TableColumn dateTimeCol = new TableColumn(configs.get(6).get("title"));
            TableColumn statusCol = new TableColumn(configs.get(7).get("title"));


            roomNumberCol.setPrefWidth(150);
            receiveNameCol.setPrefWidth(150);
            senderNameCol.setPrefWidth(150);
            widthCol.setPrefWidth(150);
            heightCol.setPrefWidth(150);
            priorityCol.setPrefWidth(150);
            dateTimeCol.setPrefWidth(150);
            statusCol.setPrefWidth(150);

            roomNumberCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(0).get("field")));
            receiveNameCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(1).get("field")));
            senderNameCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(2).get("field")));
            widthCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(3).get("field")));
            heightCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(4).get("field")));
            priorityCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(5).get("field")));
            dateTimeCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(6).get("field")));
            statusCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(7).get("field")));

            dateTimeColumn = dateTimeCol;
            roomNumberColumn = roomNumberCol ;

            table.getColumns().addAll(roomNumberCol,receiveNameCol,senderNameCol,widthCol,heightCol,priorityCol,dateTimeCol,statusCol);

            dateTimeCol.setSortType(TableColumn.SortType.DESCENDING);
            table.getSortOrder().setAll(dateTimeCol);
        }else if (mailType.equals("Parcel")){
            for(Mail m : mailFileDataSource.getMailList().getMails()){
                if(m.getName().equals(accountFileDataSource.getAccountList().getCurrentAccount().getResident().getName()) &&
                        m.getLastName().equals(accountFileDataSource.getAccountList().getCurrentAccount().getResident().getLastName())&& m.getType().equals("Parcel")){
                    userMail.add(m);
                }
            }
            mailObservableList = FXCollections.observableArrayList(userMail);
            table.setItems(mailObservableList);

            ArrayList<StringConfiguration> configs = new ArrayList<>();
            configs.add(new StringConfiguration("title:Room number", "field:roomNumber"));
            configs.add(new StringConfiguration("title:Receiver name", "field:name"));
            configs.add(new StringConfiguration("title:Sender name", "field:postman"));
            configs.add(new StringConfiguration("title:Width", "field:width"));
            configs.add(new StringConfiguration("title:Height", "field:height"));
            configs.add(new StringConfiguration("title:Company", "field:company"));
            configs.add(new StringConfiguration("title:Tracking number", "field:trackingNumber"));
            configs.add(new StringConfiguration("title:Send date time", "field:sendDateTime"));
            configs.add(new StringConfiguration("title:Status", "field:status"));


            TableColumn roomNumberCol = new TableColumn(configs.get(0).get("title"));
            TableColumn receiverNameCol = new TableColumn(configs.get(1).get("title"));
            TableColumn senderNameCol = new TableColumn(configs.get(2).get("title"));
            TableColumn widthCol = new TableColumn(configs.get(3).get("title"));
            TableColumn heightCol = new TableColumn(configs.get(4).get("title"));
            TableColumn companyCol = new TableColumn(configs.get(5).get("title"));
            TableColumn trackingNumberCol = new TableColumn(configs.get(6).get("title"));
            TableColumn dateTimeCol = new TableColumn(configs.get(7).get("title"));
            TableColumn statusCol = new TableColumn(configs.get(8).get("title"));

            roomNumberCol.setPrefWidth(150);
            receiverNameCol.setPrefWidth(150);
            senderNameCol.setPrefWidth(150);
            widthCol.setPrefWidth(150);
            heightCol.setPrefWidth(150);
            companyCol.setPrefWidth(150);
            trackingNumberCol.setPrefWidth(150);
            dateTimeCol.setPrefWidth(150);
            statusCol.setPrefWidth(150);


            roomNumberCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(0).get("field")));
            receiverNameCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(1).get("field")));
            senderNameCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(2).get("field")));
            widthCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(3).get("field")));
            heightCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(4).get("field")));
            companyCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(5).get("field")));
            trackingNumberCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(6).get("field")));
            dateTimeCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(7).get("field")));
            statusCol.setCellValueFactory(new PropertyValueFactory<>(configs.get(8).get("field")));

            dateTimeColumn = dateTimeCol;
            roomNumberColumn = roomNumberCol ;

            table.getColumns().addAll(roomNumberCol,receiverNameCol,senderNameCol,widthCol,heightCol,companyCol,trackingNumberCol,dateTimeCol,statusCol);

            dateTimeCol.setSortType(TableColumn.SortType.DESCENDING);
            table.getSortOrder().setAll(dateTimeCol);
        }
    }

    public void setTable(String mailType){
        showMailData(mailType);
        table.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                showSelectedMail(newValue);
            }
        });
    }

    private void showSelectedMail(Mail mail) {
        selectedMail = mail;
        staffReceiver.setText(mail.getStaffReceiver());
        if (mail.getResidentReceiver().equals("Empty") || mail.getReceiveFromStaff().equals("null")){
            residentReceiver.setText("Not received");
        }else{
            residentReceiver.setText(mail.getResidentReceiver());
        }
        if (mail.getReceiveFromStaff().equals("Empty") || mail.getReceiveFromStaff().equals("null")){
            receiveFrom.setText("Not received");
        }else{
            receiveFrom.setText(mail.getReceiveFromStaff());
        }
        receiveDateTime.setText(mail.getReceiveDateTime());
        mailImage.setImage(null);
        if (!selectedMail.getImage().equals("-")){
            try {
                mailImage.setImage(new Image("/mailsImage/" + selectedMail.getImage()));
            }catch (IllegalArgumentException e){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Load image Error!!");
                alert.setHeaderText(null);
                alert.setContentText("Can't load this image, Please close program and try again.");

                alert.showAndWait();
            }
        }
    }

    @FXML public void handleSelectTableTypeOnAction(ActionEvent event) throws IOException{
        table.getColumns().clear();
        userMail.clear();
        setTable(tableType.getSelectionModel().getSelectedItem());
    }
    @FXML public void handleSelectSortTypeOnAction(ActionEvent event) throws IOException{
        if (sortBy.getSelectionModel().getSelectedItem().equals("Latest mail")){
            dateTimeColumn.setSortType(TableColumn.SortType.DESCENDING);
            table.getSortOrder().setAll(dateTimeColumn);
        }else if (sortBy.getSelectionModel().getSelectedItem().equals("Ascending room number")){
            roomNumberColumn.setSortType(TableColumn.SortType.ASCENDING);
            table.getSortOrder().setAll(roomNumberColumn);
        }else if (sortBy.getSelectionModel().getSelectedItem().equals("Descending room number")){
            roomNumberColumn.setSortType(TableColumn.SortType.DESCENDING);
            table.getSortOrder().setAll(roomNumberColumn);
        }
    }
}

