package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Popup;
import javafx.stage.PopupWindow;
import javafx.stage.Stage;
import models.*;
import services.AccountFileDataSource;
import services.MailFileDataSource;
import services.RoomFileDataSource;

import java.io.*;

public class Login {
    @FXML TextField username;
    @FXML PasswordField password;
    @FXML Button enter, back, register;
    @FXML ImageView background;

    private AccountFileDataSource accountFileDataSource;
    private RoomFileDataSource roomFileDataSource;
    private MailFileDataSource mailFileDataSource;

    @FXML public void initialize() throws IOException {
        background.setImage(new Image("/images/blue sky.jpg"));
        accountFileDataSource = new AccountFileDataSource();
        accountFileDataSource.checkFile();
        roomFileDataSource = new RoomFileDataSource();
        roomFileDataSource.checkFile();
        mailFileDataSource = new MailFileDataSource();
        mailFileDataSource.checkFile();
    }

    @FXML public void handleRegisterOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/user_registration.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        UserRegistration controller = loader.getController();
        controller.setAccountFileDataSource(accountFileDataSource);
        controller.setRoomFileDataSource(roomFileDataSource);

        stage.show();
    }
    @FXML public void handleBackOnAction(ActionEvent event) throws IOException {
        Button e = (Button) event.getSource();
        Stage stage = (Stage) e.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/menu.fxml"));
        stage.setScene(new Scene(loader.load(),1280,800));

        stage.show();
    }
    @FXML public void handleEnterOnAction(ActionEvent event) throws IOException {
        for(AdminAccount adminAccount : accountFileDataSource.getAccountList().getAdminAccounts()){
            if(adminAccount.getUsername().equals(username.getText()) && adminAccount.getPassword().equals(password.getText())){
                Button e = (Button) event.getSource();
                Stage stage = (Stage) e.getScene().getWindow();

                FXMLLoader loader = new FXMLLoader(getClass().getResource("/system_for_admin.fxml"));
                stage.setScene(new Scene(loader.load(),1280,800));

                accountFileDataSource.getAccountList().setCurrentAccount(new AdminAccount(username.getText(),password.getText()));

                SystemForAdmin controller = loader.getController();
                controller.setAccountFileDataSource(accountFileDataSource);

                stage.show();
            }
        }
        for(StaffAccount staffAccount : accountFileDataSource.getAccountList().getStaffAccounts()){
            if(staffAccount.getUsername().equals(username.getText()) && staffAccount.getPassword().equals(password.getText())){
                if (staffAccount.getStatus().equals("Banned")){
                    staffAccount.bannedLogin();
                    accountFileDataSource.updateStaff();
                    accountFileDataSource.getAccountList().setCurrentAccount(new Account("null","null"));
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Login failed!!");
                    alert.setHeaderText(null);
                    alert.setContentText("Your account was banned.");

                    alert.showAndWait();
                }else{
                    Button e = (Button) event.getSource();
                    Stage stage = (Stage) e.getScene().getWindow();

                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/system_for_staff.fxml"));
                    stage.setScene(new Scene(loader.load(),1280,800));

                    accountFileDataSource.setTimeToStaff(staffAccount);
                    accountFileDataSource.getAccountList().setCurrentAccount(new StaffAccount(username.getText(),password.getText(),staffAccount.getName(),staffAccount.getLastName()));

                    SystemForStaff controller = loader.getController();
                    controller.setAccountFileDataSource(accountFileDataSource);
                    controller.setRoomFileDataSource(roomFileDataSource);
                    controller.setMailFileDataSource(mailFileDataSource);

                    stage.show();
                }
            }
        }
        for(UserAccount userAccount : accountFileDataSource.getAccountList().getUserAccounts()){
            if(userAccount.getUsername().equals(username.getText()) && userAccount.getPassword().equals(password.getText())){
                Button e = (Button) event.getSource();
                Stage stage = (Stage) e.getScene().getWindow();

                FXMLLoader loader = new FXMLLoader(getClass().getResource("/system_for_user.fxml"));
                stage.setScene(new Scene(loader.load(),1280,800));

                accountFileDataSource.getAccountList().setCurrentAccount(new UserAccount(userAccount.getResident(),username.getText(),password.getText()));

                SystemForUser controller = loader.getController();
                controller.setAccountFileDataSource(accountFileDataSource);
                controller.setRoomFileDataSource(roomFileDataSource);
                controller.setMailFileDataSource(mailFileDataSource);

                stage.show();
            }
        }

        if (accountFileDataSource.getAccountList().getCurrentAccount() == null){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Login failed!!");
            alert.setHeaderText(null);
            alert.setContentText("Please check your username and password.");

            alert.showAndWait();
        }
    }
}
